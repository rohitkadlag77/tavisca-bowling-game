﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall
{
	public class FramesProducer
	{
		public List<Frame> GetFrames(List<int> throws)
		{
			var frames = new List<Frame>();
			var frame = 1;
			var frameStart = 0;
			var numberOfthrows = throws?.Count;
			while (numberOfthrows > 0 && frames.Count < 10)
			{
				if (throws[frameStart] == 10)
				{
					frames.Add(new Strike(throws, frameStart));
					frameStart += 1;
					numberOfthrows--;

					//if (numberOfthrows <= 2)
					//	numberOfthrows -= 2;
				}
				else if (throws[frameStart] + throws[frameStart + 1] == 10)
				{
					frames.Add(new Spare(throws, frameStart));
					frameStart += 2;

					numberOfthrows -= 2;

					if (numberOfthrows <= 2)
						numberOfthrows--;
				}
				else
				{
					frames.Add(new NormalFrame(throws, frameStart));
					frameStart += 2;
					numberOfthrows -= 2;
				}
				frame++;
			}
			return frames;
		}
	}
}
