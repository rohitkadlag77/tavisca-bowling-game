﻿namespace BowlingBall.Contracts
{
	public interface ICalculateScore
	{
		int GetScore();
	}
}
