﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall.Contracts
{
	public abstract class AbstractGame : IGame, ICalculateScore
	{
		protected List<int> throws;
		public List<Frame> Frames { get; set; }
		public AbstractGame()
		{
			throws = new List<int>();
			Frames = new List<Frame>();
		}
		public abstract int GetScore();
	}
}
