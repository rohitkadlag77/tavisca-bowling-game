﻿using System.Collections.Generic;

namespace BowlingBall.Contracts
{
	public interface IGame
	{
		List<Frame> Frames { get; set; }
	}
}
