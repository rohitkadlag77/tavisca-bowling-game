﻿using BowlingBall.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingBall
{
	public class LiveGame : AbstractGame,ILiveScore
	{
		public int LiveScore { get { return Frames.Sum(a => a.LiveScore); } }
		private Frame lastFrame;
		public void Roll(int pins)
		{
			Frame frame = null;
			throws.Add(pins);
			if (lastFrame == null)
			{
				if (pins == 10 && throws.Count == 1)
				{
					lastFrame = frame = new Strike(throws, throws.Count - 1);
				}
				else if (throws.Count > 1)
				{
					var lastThrow = throws[throws.Count - 1];
					var secondLastThrow = throws[throws.Count - 2];
					var frameStart = throws.Count - 2;
					if (lastThrow + secondLastThrow == 10)
					{
						lastFrame = frame = new Spare(throws, frameStart);
					}
					else
					{
						lastFrame = frame = new NormalFrame(throws, frameStart);
					}
				}

			}
			else
			{
				var totalThrows = throws.Count;

				int newFrameStart = lastFrame.StartingThrow + lastFrame.FrameSize();
				if (pins == 10 && newFrameStart == totalThrows - 1)
				{
					lastFrame = frame = new Strike(throws, throws.Count - 1);
				}
				else if ((totalThrows - newFrameStart + 1) > 1)
				{
					var lastThrow = throws[throws.Count - 1];
					var secondLastThrow = throws[throws.Count - 2];
					if (lastThrow + secondLastThrow == 10)
					{
						lastFrame = frame = new Spare(throws, newFrameStart);
					}
					else
					{
						lastFrame = frame = new NormalFrame(throws, newFrameStart);
					}
				}
			}

			if (frame != null && Frames.Count < 10)
				Frames.Add(frame);
		}
		public override int GetScore()
		{
			return LiveScore;
		}
	}
}
