﻿using BowlingBall.Contracts;
using System.Collections;
using System.Collections.Generic;

namespace BowlingBall
{
	public class NormalFrame : Frame, ILiveScore
	{
		public NormalFrame(List<int> throws, int startFrame) : base(throws, startFrame)
		{
			//throws.Add(firstThrow);
			//throws.Add(secondThrow);
		}
		public override int LiveScore
		{
			get
			{
				return (int)throws[StartingThrow] + throws.Count - 1 >= StartingThrow + 1 ? (int)throws[StartingThrow + 1] : 0;
			}
		}

		public bool IsFrameScoringCompleted
		{
			get
			{
				return throws.Count - 1 >= StartingThrow + 1;
			}
		}

		override public int GetScore()
		{
			return (int)throws[StartingThrow] + (int)throws[StartingThrow + 1];
		}

		override public int FrameSize()
		{
			return 2;
		}
	}
}
