﻿using BowlingBall.Contracts;
using System.Collections.Generic;

namespace BowlingBall
{
	public class Game : AbstractGame
	{
		public void Roll(int pins)
		{
			throws.Add(pins);
		}
		public void Roll(int[] throwsInGame)
		{
			throws.AddRange(throwsInGame);
		}

		public override int GetScore()
		{
			//Frames = GetFramesFromPins();
			Frames = new FramesProducer().GetFrames(throws);
			return GetScoreFromFrames();
		}

		private int GetScoreFromFrames()
		{
			int total = 0;
			foreach (Frame frame in Frames)
				total += frame.GetScore();
			return total;
		}
	}
}
