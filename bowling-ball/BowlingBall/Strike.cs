﻿using BowlingBall.Contracts;
using System.Collections.Generic;

namespace BowlingBall
{
	public class Strike : Frame, ILiveScore
	{
		public Strike(List<int> throws, int frameStart) : base(throws, frameStart)
		{
		}
		public override int LiveScore
		{
			get
			{
				return 10 + FirstBonusBall() + SecondBonusBall();
			}
		}

		public bool IsFrameScoringCompleted
		{
			get
			{
				return throws.Count - 1 > StartingThrow + FrameSize() + 1;
			}
		}
		override public int GetScore()
		{
			return 10 + FirstBonusBall() + SecondBonusBall();
		}
		override public int FrameSize()
		{
			return 1;
		}
	}

}
