﻿using BowlingBall.Contracts;
using System.Collections.Generic;

namespace BowlingBall
{
	public abstract class Frame : ICalculateScore, ILiveScore
	{
		protected List<int> throws;
		public int StartingThrow { get; set; }
		public int Score { get; set; }

		public abstract int LiveScore { get; }

		public Frame(List<int> throws, int frameStart)
		{
			this.throws = throws;
			this.StartingThrow = frameStart;
		}

		public abstract int GetScore();
		abstract public int FrameSize();

		protected int FirstBonusBall()
		{
			return throws.Count - 1 < StartingThrow + FrameSize() ? 0 : throws[StartingThrow + FrameSize()];
		}

		protected int SecondBonusBall()
		{
			return throws.Count - 1 < StartingThrow + FrameSize() + 1 ? 0 : throws[StartingThrow + FrameSize() + 1];
		}
	}
}
