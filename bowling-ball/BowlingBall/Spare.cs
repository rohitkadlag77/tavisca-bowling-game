﻿using BowlingBall.Contracts;
using System.Collections;
using System.Collections.Generic;

namespace BowlingBall
{
	public class Spare : Frame, ILiveScore
	{
		public Spare(List<int> throws, int startFrame) : base(throws, startFrame)
		{
		}
		public override int LiveScore
		{
			get
			{
				return 10 + FirstBonusBall();
			}
		}
		public bool IsFrameScoringCompleted
		{
			get
			{
				return throws.Count - 1 > StartingThrow + FrameSize() + 1;
			}
		}
		override public int GetScore()
		{
			return 10 + FirstBonusBall(); ;
		}

		override public int FrameSize()
		{
			return 2;
		}
	}
}
