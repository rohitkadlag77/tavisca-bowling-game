﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingBall.Tests
{
	[TestClass]
	public class LiveGameFixtures
	{
		[TestMethod]
		public void Test_LiveGame_AllSpare_equals_150()
		{
			var liveGame = new LiveGame();
			for (int i = 0; i < 21; i++)
			{
				liveGame.Roll(5);
			}
			Assert.AreEqual(150, liveGame.LiveScore);
		}

		[TestMethod]
		public void TestAll_Strikes_should_equal_300()
		{
			var liveGame = new LiveGame();
			for (int i = 0; i < 12; i++)
			{
				liveGame.Roll(10);
			}
			var score = liveGame.LiveScore;
			Assert.AreEqual(300, score);
		}
	}
}
