﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingBall.Tests
{
	[TestClass]
	public class GameFixture
	{
		[TestMethod]
		public void Gutter_game_score_should_be_zero_test()
		{
			var game = new Game();
			Roll(game, 0, 20);
			Assert.AreEqual(0, game.GetScore());
		}

		[TestMethod]
		public void Example_Test()
		{
			var game = new Game();
			game.Roll(new int[17] { 10, 9, 1, 5, 5, 7, 2, 10, 10, 10, 9, 1, 8, 2, 9, 1, 10 });
			var score = game.GetScore();
			Assert.AreEqual(197, score);
		}

		[TestMethod]
		public void TestAll_Strikes_should_equal_300()
		{
			var game = new Game();
			game.Roll(new int[12] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 });
			var score = game.GetScore();
			Assert.AreEqual(300, score);
		}

		[TestMethod]
		public void TestFinal_Spare_equals275()
		{
			var game = new Game();
			game.Roll(new int[12] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 10 });
			var score = game.GetScore();
			Assert.AreEqual(275, score);
		}

		[TestMethod]
		public void TestFinal_AllSpare_equals_150()
		{
			var game = new Game();
			game.Roll(new int[21] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
			var score = game.GetScore();
			Assert.AreEqual(150, score);
		}

		[TestMethod]
		public void Test_TotalFrames_Score()
		{
			var game = new Game();
			game.Roll(new int[6] { 10, 2, 7, 3, 7, 10 });
			var score = game.GetScore();
			Assert.AreEqual(48, score);
			Assert.AreEqual(3, game.Frames.Count);
		}

		private void Roll(Game game, int pins, int times)
		{
			for (int i = 0; i < times; i++)
			{
				game.Roll(pins);
			}
		}
	}
}
